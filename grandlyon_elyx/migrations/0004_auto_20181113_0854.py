from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grandlyon_elyx', '0003_auto_20181112_1722'),
    ]

    operations = [
        migrations.AddField(
            model_name='grandlyonelyx',
            name='password',
            field=models.CharField(default='password', max_length=128, verbose_name='Password'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grandlyonelyx',
            name='scope',
            field=models.CharField(default='scope', max_length=128, verbose_name=b'Scope du service'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grandlyonelyx',
            name='service_url',
            field=models.URLField(default='service_url', max_length=256, verbose_name=b'URL du service'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grandlyonelyx',
            name='username',
            field=models.CharField(default='username', max_length=128, verbose_name='Username'),
            preserve_default=False,
        ),
    ]
