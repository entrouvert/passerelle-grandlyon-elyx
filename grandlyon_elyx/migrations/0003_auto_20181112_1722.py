from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grandlyon_elyx', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='grandlyonelyx',
            name='wsdl_url',
        ),
        migrations.AlterField(
            model_name='grandlyonelyx',
            name='log_level',
            field=models.CharField(
                default=b'INFO',
                max_length=10,
                verbose_name='Log Level',
                choices=[
                    (b'NOTSET', b'NOTSET'),
                    (b'DEBUG', b'DEBUG'),
                    (b'INFO', b'INFO'),
                    (b'WARNING', b'WARNING'),
                    (b'ERROR', b'ERROR'),
                    (b'CRITICAL', b'CRITICAL'),
                ],
            ),
        ),
    ]
